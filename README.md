This repository contains resources for understanding homotopy type theory. Other online resources include:
- Homotopy Type Theory Course with Videos by Robert Harper: http://www.cs.cmu.edu/~rwh/courses/hott
- A nice website with some intuitive explanations: http://www.science4all.org/article/homotopy-type-theory/