data Bool : Set where
  true : Bool
  false : Bool
{-# BUILTIN BOOL Bool #-}
{-# BUILTIN TRUE true #-}
{-# BUILTIN FALSE false #-}

not : Bool -> Bool
not false = true
not true = false

data _+_ (A B : Set) : Set where
  inl : A -> A + B
  inr : B -> A + B

if_then_else_ : {A B : Set} -> Bool -> A -> B -> A + B
if true then a else _ = inl a
if false then _ else b = inr b

rec_sum : {A B C : Set} -> (A -> C) -> (B -> C) -> A + B -> C
rec_sum f _ (inl a) = f a
rec_sum _ g (inr b) = g b

data ℕ : Set where
  zero : ℕ
  succ : ℕ -> ℕ
{-# BUILTIN NATURAL ℕ #-}
