data Bool : Set where
  true : Bool
  false : Bool

not : Bool -> Bool
not true = false
not false = true

equiv : Bool -> Bool -> Bool
equiv true true = true
equiv true false = false
equiv false true = false
equiv false false = true

_||_ : Bool -> Bool -> Bool
_ || true = true
true || _ = true
_ || _ = false

data Nat : Set where
  zero : Nat
  succ : Nat -> Nat

pred : Nat -> Nat
pred zero = zero
pred (succ n) = n

_+_ : Nat -> Nat -> Nat
zero + m = m
succ n + m = succ (n + m)

_*_ : Nat -> Nat -> Nat
zero * m = zero
succ n * m = (n * m) + m

_-_ : Nat -> Nat -> Nat
zero - _ = zero
n - zero = n
succ n - m = succ (n - m)

_<_ : Nat -> Nat -> Bool
_ < zero = false
zero < _ = true
succ n < succ m = n < m

id : {A : Set} -> A -> A
id = \x -> x

K : (A B : Set) -> A -> B -> A
K _ _ x _ = x

S : (A B C : Set) -> (A -> B -> C) -> (A -> B) -> A -> C
S _ _ _ f g x = f x (g x)
