open import Agda.Primitive

data Bool : Set where
  true false : Bool
{-# BUILTIN BOOL Bool #-}
{-# BUILTIN TRUE true #-}
{-# BUILTIN FALSE false #-}

record Unit : Set where
{-# BUILTIN UNIT Unit #-}

data Nat : Set where
  zero : Nat
  succ : Nat -> Nat
{-# BUILTIN NATURAL Nat #-}



data List {n : Level} (A : Set n) : Set n where
  [] : List A
  _::_ : A -> List A -> List A

map : {A B : Set} -> (A -> B) -> List A -> List B
map f [] = []
map f (x :: xs) = f x :: map f xs


if_then_else_ : {C : Set} -> Bool -> C -> C -> C
if true then a else _ = a
if false then _ else b = b

filter : {A : Set} -> (A -> Bool) -> List A -> List A
filter f [] = []
filter f (x :: xs) = if f x then (x :: (filter f xs)) else (filter f xs)

foldl : {A : Set} -> (A -> A -> A) -> List A -> A -> A
foldl f [] s = s
foldl f (x :: xs) s = foldl f xs (f x s)

data _×_ (A B : Set) : Set where
  <_,_> : A -> B -> A × B

fst : {A B : Set} -> A × B -> A
fst < a , _ > = a

snd : {A B : Set} -> A × B -> B
snd < _ , b > = b

zip : {A B : Set} -> List A -> List B -> List (A × B)
zip (x :: xs) (y :: ys) = < x , y > :: zip xs ys
zip _ _ = []

data enum (A B : Set) : Set where
  inl : A -> enum A B
  inr : B -> enum A B

case : {A B C : Set} -> (A -> C) -> (B -> C) -> (enum A B -> C)
case f _ (inl a) = f a
case _ g (inr b) = g b

Vecr : Set -> Nat -> Set
Vecr A zero = Unit
Vecr A (succ n) = A × Vecr A n

zipvr : {n : Nat} {A B : Set} -> Vecr A n -> Vecr B n -> Vecr (A × B) n
zipvr {0} unit unit = unit
zipvr {succ n} < a , ra > < b , rb > = < < a , b > , zipvr ra rb >

headr : {n : Nat} {A : Set} -> Vecr A (succ n) -> A
headr < a , _ >  = a

data Vec {n : Level} (A : Set n) : Nat -> Set n where
  <> : Vec A zero
  _::_ : {n : Nat} -> A -> Vec A n -> Vec A (succ n)

data Fin : Nat -> Set where
  fzero : { n : Nat } -> Fin (succ n)
  fsucc : { n : Nat } -> Fin n -> Fin (succ n)

_!_ : {n : Nat} {A : Set} -> Vec A n -> Fin n -> A
<> ! ()
(x :: xs) ! fzero = x
(x :: xs) ! fsucc i = xs ! i

_!!_ : {n : Nat} {A : Set} -> Vec A (succ n) -> Fin (succ n) -> A
(x :: xs) !! fzero = x
(x :: (y :: xs)) !! fsucc i  = (y :: xs) !! i

data DBTree (A : Set) : Nat -> Set where
  dleaf : A -> DBTree A 0
  dnode : {n : Nat} -> DBTree A n -> DBTree A n -> DBTree A (succ n)

data HBTree (A : Set) : Nat -> Set where
  hleaf : A -> HBTree A 0
  hnode : {n : Nat} -> HBTree A n -> HBTree A n -> HBTree A (succ n)
  hleft : {n : Nat} -> HBTree A (succ n)-> HBTree A n -> HBTree A (succ (succ n))
  hrght : {n : Nat} -> HBTree A n -> HBTree A (succ n) -> HBTree A (succ (succ n))
